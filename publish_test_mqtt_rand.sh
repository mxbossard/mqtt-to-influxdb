#! /bin/sh

./msg_producer.py | mosquitto_pub -l -h mydockerweb.local -t public/foo -q 0 &
./msg_producer.py | mosquitto_pub -l -h mydockerweb.local -t public/bar -q 1 &
./msg_producer.py | mosquitto_pub -l -h mydockerweb.local -t public/baz -q 2 &

sleepTime=0.1
i=0; while true; do echo $i; i=$((i+1)); sleep $sleepTime; done | mosquitto_pub -t public/counter_qos1 -h mydockerweb.local -q 1 -l &
i=0; while true; do echo $i; i=$((i+1)); sleep $sleepTime; done | mosquitto_pub -t public/counter_qos2 -h mydockerweb.local -q 2 -l &
