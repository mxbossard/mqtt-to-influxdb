FROM python:${PYTHON_IMAGE_TAG:-3.8-alpine}
 
RUN apk update \
	&& apk --no-cache --update add build-base

RUN pip install paho-mqtt influxdb-client
RUN apk del build-base
RUN pip install requests

ADD mqtt_daemon.py ./

ENTRYPOINT [ "python", "./mqtt_daemon.py" ]
