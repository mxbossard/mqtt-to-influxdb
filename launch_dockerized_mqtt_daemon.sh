#! /bin/sh

DEFAULT_HOST="mydockerweb.local"
MQTT_HOST=$DEFAULT_HOST
MQTT_TOPIC="public/#"
INFLUXDB_URL=http://influxdb.$DEFAULT_HOST
INFLUXDB_DB_NAME=mqtt_public

params=${@:-$MQTT_HOST $MQTT_TOPIC $INFLUXDB_URL $INFLUXDB_DB_NAME}

echo Using params: [$params]

#curl -k -X POST "$INFLUXDB_URL/query" --data-urlencode "q=CREATE DATABASE $INFLUXDB_DB_NAME"

docker run -it --rm --name test-mqtt-daemon.sh --network=host -v "$PWD":/usr/src/myapp -w /usr/src/myapp --entrypoint=python mqtt-to-influxdb mqtt_daemon.py $params
