# A dockerized MQTT to InfluxDB daemon

A really simple daemon which subscribe to a MQTT Topic and store numeric messages into InfluxDB.

## Principle
The daemon subscribe to a MQTT topic (which can contain a wildcard). When a message with a numeric value is received, publish it to influxdb with tags :
* source: "mqtt_daemon"
* topic: the original mqtt topic

## Usage
The Daemon take some parameters :
1. MQTT host
1. MQTT topic to subscribe (can contain a wildcard)
1. INFLUXDB URL
1. INFLUXDB database where to store data

## Tests
Launch *./launch_dockerized_mqtt_daemon.sh* which run the daemon insice a docker container.

## Caveats
* No reconnection management
* Poor data format management : not a number => not stored

