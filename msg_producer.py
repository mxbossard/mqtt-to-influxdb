#! /usr/bin/python

# need packages: cayenneLPP

from __future__ import print_function
import random
import time
import sys
import getopt

def rawData(value):
    print(str(value))
    sys.stdout.flush()

def cayenneLppData(value):
    pass

def produceUniformRandomData(a, b):
    return random.uniform(a, b)

def produceGaussianRandomData(a, b, mu, sigma):
    val = None
    while (val < 0 or val > 1):
        val = random.gauss(mu, sigma)

    return val * (b - a) + a

latestContinuedValue = None
def produceContinuedRandomData(a, b, sigma):
    global latestContinuedValue
    if not latestContinuedValue:
        latestContinuedValue = produceUniformRandomData(a, b)
    
    mu = (latestContinuedValue - a) / (b - a)
    latestContinuedValue = produceGaussianRandomData(a, b, mu, sigma * random.random() * 3)
    return latestContinuedValue

def usage():
    print('msg_producer.py [DISTRIBUTION] [OUTPUT FORMAT] [OUTPUT RANGE] [INPUT CONFIG]')
    print('')
    print('--- DISTRIBUTION ---')
    print('-u : Uniform')
    print('-g sigma : Gaussian')
    print('-c sigma : Continue by default with sigma = 0.02')
    print('')
    print('--- OUTPUT FORMAT ---')
    print('-i : Integer by default')
    print('-f : Floating')
    print('')
    print('--- OUTPUT RANGE ---')
    print('-a : Minimal output 0 by default')
    print('-b : Maximal output 42 by default')
    print('')
    print('--- INPUT CONFIG ---')
    print('-s : Random seed (to reproduce series)')
    print('-p : Period between sampbles (in seconds) 1sec by default')
    print('')

def main(argv):

    numericMin = 0
    numericMax = 42
    gaussianSigma = 0.02
    outputFormater = lambda x: int(x)
    randomFunc = lambda a, b : produceContinuedRandomData(a, b, gaussianSigma)
    seed = None
    periodInSec = 1

    try:
        opts, args = getopt.getopt(argv,"hug:c:ifa:b:s:p:")
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()

        elif opt == '-u':
            # Uniform random distribution
            randomFunc = lambda a, b : produceUniformRandomData(a, b)

        elif opt == '-g':
            # Gaussian random distribution
            mu = 0.5
            gaussianSigma = float(arg)
            randomFunc = lambda a, b : produceGaussianRandomData(a, b, mu, gaussianSigma)

        elif opt == '-c':
            # Gaussian random distribution
            gaussianSigma = float(arg)
            randomFunc = lambda a, b : produceContinuedRandomData(a, b, gaussianSigma)

        elif opt == '-i':
            # integer output
            outputFormater = lambda x : int(x)

        elif opt == '-f':
            # floating output
            outputFormater = lambda x : float(x)

        elif opt == '-a':
            # min value
            numericMin = int(arg)

        elif opt == '-b':
            # max value
            numericMax = int(arg)

        elif opt == '-s':
            # random seed
            seed = arg

        elif opt == '-p':
            # producing period
            periodInSec = float(arg)

    random.seed(seed)

    while 1:
        #value = produceRandomData(numericMin, numericMax)
        value = randomFunc(numericMin, numericMax)
        formatted = outputFormater(value)
        rawData(formatted)
        time.sleep(periodInSec)

try:
    if __name__ == "__main__":
        main(sys.argv[1:])
except KeyboardInterrupt:
    sys.exit(0)
